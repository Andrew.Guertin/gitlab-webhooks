#!/usr/bin/env python2

"""
A lightweight HTTPS server for use with GitLab webhooks

source: https://gitlab.uvm.edu/saa/gitlab-webhooks
author: Brian O'Donnell <brian.odonnell@uvm.edu>
"""

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import json
import re
import ssl
import subprocess
from fabric.api import *
import logging
import smtplib # For sending deploy emails
import time # For getting the current time when sending deploy emails

# Customize this string before use!!!
SECRET_KEY = 'IMX9JQFC8W'

# Port to bind the http server to
LISTEN_PORT = 8000

# Path to SSL certificate
CERT_FILE = '/users/w/e/webmster/gitlab-webhooks/webhook-server.pem'


class WebhookHandler(BaseHTTPRequestHandler):
    """
    An inheritable class which implements a basic GitLab webhooks API
    """
    def exec_push(self, data):
        raise NotImplementedError('exec_push')

    def exec_tag_push(self, data):
        raise NotImplementedError('exec_tag_push')

    def exec_note(self, data):
        raise NotImplementedError('exec_note')

    def exec_issue(self, data):
        raise NotImplementedError('exec_issue')

    def exec_merge_request(self, data):
        raise NotImplementedError('exec_merge_request')

    def exec_build(self, data):
        raise NotImplementedError('exec_build')

    def respond(self, code, message):
        """
        Sends a customizable HTTP response
        """
        self.send_response(code)
        self.send_header("Content-type", "text")
        self.send_header("Content-length", str(len(message)))
        self.end_headers()
        self.wfile.write(message)

    def do_POST(self):
        """
        Handles POST requests
        """
        try:
            # slurp in the request body
            self.rfile._sock.settimeout(5)
            content = self.rfile.read(int(self.headers['Content-Length']))
            postdata = json.loads(content)

            # parse out the 'secret key' from the url path
            m = re.match('\/([A-Za-z0-9]+)\/?$', self.path)

            if m:
                # Force the user to set a custom secret key
                if SECRET_KEY == 'SUPERSECRETALPHANUMERICSTRING' or SECRET_KEY == '' or SECRET_KEY is None:
                    raise ValueError('Please set a secret key!')

                if m.group(1) != SECRET_KEY:
                    self.respond(400, 'Invalid secret key')
                elif 'object_kind' in postdata:
                    action = postdata['object_kind']

                    if action == 'push':
                        self.respond(200, 'OK')
                        self.exec_push(postdata)
                    elif action == 'tag_push':
                        self.respond(200, 'OK')
                        self.exec_tag_push(postdata)
                    elif action == 'issue':
                        self.respond(200, 'OK')
                        self.exec_issue(postdata)
                    elif action == 'note':
                        self.respond(200, 'OK')
                        self.exec_note(postdata)
                    elif action == 'merge_request':
                        self.respond(200, 'OK')
                        self.exec_merge_request(postdata)
                    elif action == 'build':
                        self.respond(200, 'OK')
                        self.exec_build(postdata)
                    else:
                        self.respond(400, 'Invalid object_kind attribute')
                        return

                else:
                    self.respond(400, 'Missing object_kind attribute')
            else:
                self.respond(400, 'Missing or invalid secret key in url')
        except NotImplementedError as e:
            self.respond(501, 'Method %s not implemented' % e)
        except (RuntimeError, ValueError) as e:
            self.respond(500, '%s' % e)


def update_git_files(branch, after, alias):
    logging.info("I want to autodeploy a change to newrev: %s in environment: %s" % (after, branch))
    run('git --git-dir=/var/www/%s.drup-lb.uvm.edu/.git fetch neworigin' % alias)
    run('git --work-tree=/var/www/{alias}.drup-lb.uvm.edu/ --git-dir=/var/www/{alias}.drup-lb.uvm.edu/.git checkout neworigin/{branch}'.format(alias=alias, branch=branch))
    run('git --work-tree=/var/www/%s.drup-lb.uvm.edu/ --git-dir=/var/www/%s.drup-lb.uvm.edu/.git reset --hard' % (alias, alias))


class CustomHookHandler(WebhookHandler):
    """
    Override parent class' exec_ methods here
    """

    def update_db(self, env):
        subprocess.call('drush @%s sql-drop --yes' % env, shell=True)
        test = subprocess.call('drush sql-sync @prod @%s --yes' % env, shell=True)
        logging.info(test)

    def exec_tag_push(self, data):
        logging.info("Saw a tag push for tag %s" % data["ref"])

        # If the tag is not a "uvm-###" tag, do no deploy it
        deploytag = data["ref"].find("refs/tags/uvm-") == 0
        nomaintain = data["commits"][-1]["message"].find("#nomaint") == -1

        if not deploytag:
                logging.info("Tag name %s does not start with 'uvm-', not deploying it" % data["ref"])
        else:
                alias = "prod"
                branch = "production"
                after = data['checkout_sha']

                logging.info("Sending deploy email")
                self.__send_deploy_email(data)

                logging.info("Clearing cache")
                local('drush @%s cc all' % alias)

                if nomaintain:
                    logging.info("Entering maintenance mode")
                    local('drush @%s vset maintenance_mode 1' % alias)
                else:
                    logging.info("#nomaint in top commit, not entering maintenance mode")

                logging.info("Performing git checkout to commit %s" % after)
                results = execute(update_git_files, "production", after, alias, hosts=['drup-ws1', 'drup-ws2'])

                logging.info("Performing database updates")
                local('drush @%s updatedb --yes' % alias)

                logging.info("Exiting maintenance mode")
                local('drush @%s vset maintenance_mode 0' % alias)

                logging.info("Clearing cache")
                local('drush @%s cc all' % alias)

                logging.info("Sucessfully finished deployment to Production")

    def __update_test_instance(self, instance, commit, syncdb, nomaint):
        """
        Updates an instance's database and files to live, and code to
        the given commit.
        """
        branch = instance

        logging.info("--------")
        logging.info("Going to deploy to %s" % instance)

        if syncdb:
            logging.info("Syncing database from production to %s" % instance)
            self.update_db(instance)
        else:
            logging.info("#nosyncdb in top commit, not syncing database")

        if nomaint:
            logging.info("Entering maintenance mode on %s" % instance)
            local('drush @%s vset maintenance_mode 1' % instance)
        else:
            logging.info("#nomaint in top commit, not entering maintenance mode")

        logging.info("Updating git repository on %s" % instance)
        results = execute(update_git_files, branch, commit, instance, hosts=['localhost'])

        logging.info("Running database updates on %s" % instance)
        local('drush @%s updatedb --yes' % instance)

        if nomaint:
            logging.info("Exiting maintenance mode on %s" % instance)
            local('drush @%s vset maintenance_mode 0' % instance)

        logging.info("Clearing cache on %s" % instance)
        local('drush @%s cc all' % instance)

        logging.info("Syncing files on %s" % instance)
#        subprocess.call('rsync -r --progress --delete webmster@drup-ws1.uvm.edu:/var/www/prod.drup-lb.uvm.edu/sites/default/files /var/www/%s.drup-lb.uvm.edu/sites/default/' % instance, shell=True)
	subprocess.call('rsync -av --no-owner --itemize-changes --delete webmster@drup-ws1.uvm.edu:/var/www/prod.drup-lb.uvm.edu/sites/default/files /var/www/%s.drup-lb.uvm.edu/sites/default/' % instance, shell=True)

        logging.info("Sucessfully Finished deployment to %s" % instance)

    def exec_push(self, data):
        branch = data["ref"].replace("refs/heads/", "")
        after = data["after"]
        # If the "#nosyncdb" string is in the commit message, tell the deploy
        # function not to sync the database.
        syncdb = data["commits"][-1]["message"].find("#nosyncdb") == -1
        
        # If the "#nomaint" string is in the commit message, tell the deploy
        # function not to enter maintenance mode.
        nomaint = data["commits"][-1]["message"].find("#nomaint") == -1

        if branch in ["staging", "qa"]:
            self.__update_test_instance(branch, after, syncdb, nomaint)
        else:
            logging.info("Saw a push to branch %s, ignoring it" % branch)

    def __send_deploy_email(self, data):
        # Get the new tag, knowing that we can't get here unless it starts with "uvm-"
        ref = data["ref"]
        tag = ref[ref.find("uvm-"):]
        # Get the old tag
        oldtag = "uvm-" + str(int(tag[4:]) - 1)
        # gitlab does not tell us the time of the push(?), so use the current time
        currtime = time.ctime()

        link = "https://gitlab.uvm.edu/webteam/uvm-drupal/compare/" + oldtag + "..." + tag
        user = data["user_name"]

        sender = "Drupal Deploy Script <webmster@uvm.edu>"
        to = ["UVM Web Team <webteam@uvm.edu>"]
        message = """\
From: %s
To: %s
Subject: Drupal Deploy %s

At %s, %s was deployed by %s. View the list of changes:
%s
""" % (sender, ", ".join(to), tag, currtime, tag, user, link)

        server = smtplib.SMTP("localhost")
        server.sendmail(sender, ", ".join(to), message)
        server.quit()

def main():
    """
    Starts the webserver
    """
    try:
        logging.basicConfig(level=logging.INFO,
                            filename="drupal-deploy.log", filemode="a+",
                            format="%(asctime)-15s %(levelname)-8s %(message)s")
        server = HTTPServer(('', LISTEN_PORT), CustomHookHandler)
        server.socket = ssl.wrap_socket(server.socket, certfile=CERT_FILE, server_side=True)
        server.serve_forever()
    except KeyboardInterrupt:
        logging.info('Ctrl-C pressed, shutting down.')
    except:
        e = sys.exc_info()[0]
        logging.info("Error: %s" % e)
    finally:
        server.socket.close()

if __name__ == '__main__':
    main()
